<?php


include_once dirname(__FILE__) . '/member_types.php';
include_once dirname(__FILE__) . '/custom_dropdowns.php';
include_once dirname(__FILE__) . '/custom_tabs.php';

//Add User-People Profile Fields


add_action( 'show_user_profile', 'show_tabbed_profile_fields' );
add_action( 'edit_user_profile', 'show_tabbed_profile_fields' );
 
function show_tabbed_profile_fields( $user ) { 


?>

    <h3>Faculty Profile Page Information</h3>
    <table class="form-table">
		<?php member_type_field(); ?>
		<?php edudms_pt_create_dropdown_on_user_profile_editing_area(); ?>
		<?php edudms_pt_create_shortfield_fields_on_user_profile_editing_area(); ?>
		<?php edudms_pt_create_tab_fields_on_user_profile_editing_area(); ?>
		<?php edudms_pt_create_checkbox_on_user_profile_editing_area(); ?>
        
				<?php if ( get_option( 'edudms_pt_allow_custom_links' ) == 1 ) { ?>
		<tr>
            <th><label>Custom Link URL:</label></th>
             <td>
				<input type="text" id="edudms_pt_customlink" name="edudms_pt_customlink" size="60" value="<?php echo get_the_author_meta( 'edudms_pt_customlink', $user->ID ); ?>">
                <span class="description">Please enter a URL you may redirect your links to.</span>
				<input type="checkbox" id="edudms_pt_linkactive" name="edudms_pt_edudms_pt_linkactive" value="checked" checked="checked" />
            </td>
        </tr>
		<?php } ?>
		
    </table>
	
	
<?php }





function validate_wfu_phone () {
	//preg_replace(^1
}






 
function edudms_pt_save_extra_profile_fields( $user_id ) {
 
    if ( !current_user_can( 'edit_user', $user_id ) )
        return false;
	update_tabs_user_meta();
	update_user_meta( absint( $user_id ), 'edudms_pt_member_type', wp_kses_post( $_POST['edudms_pt_member_type'] ) );
edudms_pt_change_role( $GLOBALS['user_id'] );
	update_customdropdowns_user_meta();
	update_shortfields_user_meta();
	update_checkboxes_user_meta();
	if ( get_option( 'edudms_pt_customtab2_field_setting' ) == 1 ) {
	update_user_meta( absint( $user_id ), 'edudms_pt_customtab2', wp_kses_post( $_POST['edudms_pt_customtab2'] ) ); }
	if ( get_option( 'edudms_pt_customshortfield1_field_setting' ) == 1 ) {
	update_user_meta( absint( $user_id ), 'edudms_pt_customshortfield1', wp_kses_post( $_POST['edudms_pt_customshortfield1'] ) ); }
	if ( get_option( 'edudms_pt_customshortfield2_field_setting' ) == 1 ) {
	update_user_meta( absint( $user_id ), 'edudms_pt_customshortfield2', wp_kses_post( $_POST['edudms_pt_customshortfield2'] ) ); }
	update_user_meta( $user_id, 'full_name', $_POST['first_name'] . ' ' . $_POST['last_name']  );
	update_user_meta( $user_id, 'comma_name', $_POST['last_name'] . ', ' . $_POST['first_name']  );
	update_user_meta( $user_id, 'email', $_POST['email']);
	if ( get_option( 'edudms_pt_allow_custom_links' ) == 1 ) {
	update_user_meta( $user_id, 'edudms_pt_linkactive', $_POST['edudms_pt_linkactive'] ); 
	update_user_meta( $user_id, 'edudms_pt_customlink', $_POST['edudms_pt_customlink']); }
}

add_action( 'personal_options_update', 'edudms_pt_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'edudms_pt_save_extra_profile_fields' );


function edudms_pt_add_contact_fields( $contact_methods ) {
	$contact_methods['title'] = 'Title';
	$contact_methods['phone'] = 'Phone';
	$contact_methods['office'] = 'Office Location';
	// $contact_methods['jabber'] = ''; // just removes label, have to find how to remove it.
	return $contact_methods;
};
add_filter( 'user_contactmethods', 'edudms_pt_add_contact_fields' );


//adds a custom link that can be used to link to another site instead of a profile page.
function custom_people_links() {
	
}






//Kill Stuff on Profile Page (editing side)

function hide_personal_options(){
echo "\n" . '<script type="text/javascript">jQuery(document).ready(function($) { $(\'form#your-profile > h3:first\').hide(); $(\'form#your-profile > table:first\').hide(); $(\'form#your-profile\').show(); });</script>' . "\n";

}
add_action('admin_head','hide_personal_options');

//don't kill account management if admin

require_once(ABSPATH . 'wp-includes/pluggable.php');

if(current_user_can('remove_users') !== true) {


function hide_account_management_options(){
echo "\n" . '<script type="text/javascript">jQuery(document).ready(function($) { $(\'tr#password\').hide(); });</script>' . "\n";
}
add_action('admin_head','hide_account_management_options');



}
add_action( 'personal_options', array ( 'T5_Hide_Profile_Bio_Box', 'start' ) );
/**
 * Captures the part with the biobox in an output buffer and removes it.
 *
 * @author Thomas Scholz, <info@toscho.de>
 *
 */
class T5_Hide_Profile_Bio_Box
{
    /**
     * Called on 'personal_options'.
     *
     * @return void
     */
    public static function start()
    {
        $action = ( IS_PROFILE_PAGE ? 'show' : 'edit' ) . '_user_profile';
        add_action( $action, array ( __CLASS__, 'stop' ) );
        ob_start();
    }

    /**
     * Strips the bio box from the buffered content.
     *
     * @return void
     */
    public static function stop()
    {
        $html = ob_get_contents();
        ob_end_clean();

        // remove the headline
        $headline = __( IS_PROFILE_PAGE ? 'About Yourself' : 'About the user' );
        $html = str_replace( '<h3>' . $headline . '</h3>', '', $html );

        // remove the table row
        $html = preg_replace( '~<tr class="user-description-wrap">\s*<th><label for="description".*</tr>~imsUu', '', $html );
        print $html;
    }
}

		

		
?>