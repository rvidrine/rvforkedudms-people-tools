<?php



// Register Custom Post Type
function fields_post_type() {

	$labels = array(
		'name'                  => _x( 'Fields', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Field', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Fields', 'text_domain' ),
		'name_admin_bar'        => __( 'Field', 'text_domain' ),
		'archives'              => __( 'Field Archives', 'text_domain' ),
		'parent_field_colon'     => __( 'Parent Field:', 'text_domain' ),
		'all_fields'             => __( 'All Fields', 'text_domain' ),
		'add_new_field'          => __( 'Add New Field', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_field'              => __( 'New Field', 'text_domain' ),
		'edit_field'             => __( 'Edit Field', 'text_domain' ),
		'update_field'           => __( 'Update Field', 'text_domain' ),
		'view_field'             => __( 'View Field', 'text_domain' ),
		'search_fields'          => __( 'Search Field', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_field'      => __( 'Insert into field', 'text_domain' ),
		'uploaded_to_this_field' => __( 'Uploaded to this field', 'text_domain' ),
		'fields_list'            => __( 'Fields list', 'text_domain' ),
		'fields_list_navigation' => __( 'Fields list navigation', 'text_domain' ),
		'filter_fields_list'     => __( 'Filter fields list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Field', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'page-attributes', 'revisions' ),
		'taxonomies'            => array( ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => 'edudms_ptslug',
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'field', $args );

}
add_action( 'init', 'fields_post_type', 0 );









// Register Field Type Taxonomy
function field_type() {

	$labels = array(
		'name'                       		=> _x( 'Field Types', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              		=> _x( 'Field Type', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  		=> __( 'Taxonomy', 'text_domain' ),
		'all_field types'                  => __( 'All Field Types', 'text_domain' ),
		'parent_field type'                => __( 'Parent Field Type', 'text_domain' ),
		'parent_field type_colon'          => __( 'Parent Field Type:', 'text_domain' ),
		'new_field type_name'              => __( 'New Field Type Name', 'text_domain' ),
		'add_new_field type'               => __( 'Add New Field Type', 'text_domain' ),
		'edit_field type'                  => __( 'Edit Field Type', 'text_domain' ),
		'update_field type'                => __( 'Update Field Type', 'text_domain' ),
		'view_field type'                  => __( 'View Field Type', 'text_domain' ),
		'separate_field types_with_commas' => __( 'Separate field types with commas', 'text_domain' ),
		'add_or_remove_field types'        => __( 'Add or remove field types', 'text_domain' ),
		'choose_from_most_used'      		=> __( 'Choose from the most used', 'text_domain' ),
		'popular_field types'              => __( 'Popular Field Types', 'text_domain' ),
		'search_field types'               => __( 'Search Field Types', 'text_domain' ),
		'not_found'                  		=> __( 'Not Found', 'text_domain' ),
		'no_terms'                   		=> __( 'No field types', 'text_domain' ),
		'field types_list'                 => __( 'Field Types list', 'text_domain' ),
		'field types_list_navigation'      => __( 'Field Types list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'field_type', array( 'field' ), $args );
	
//Populate taxonomy with terms
	wp_insert_term( 'Tab', 'field_type' , array( 'slug' => 'tab' ) );
	wp_insert_term( 'Shortfield', 'field_type' );
	wp_insert_term( 'Dropdown', 'field_type' );
	wp_insert_term( 'Checkbox', 'field_type' );
	
}
add_action( 'init', 'field_type', 0 );


//Metabox for taxonomy
add_action( 'do_meta_boxes', 'metabox_field_type' );

function metabox_field_type() {
	remove_meta_box('slugdiv', 'field', 'normal' );
	remove_meta_box('authordiv', 'field', 'normal' );
	remove_meta_box('pageparentdiv', 'field', 'side' );
	remove_meta_box('tagsdiv-post_tag', 'field', 'side' );
	remove_meta_box('categorydiv', 'field', 'side' );
	remove_meta_box('revisionsdiv', 'field', 'side' );
	//remove_meta_box('field_typediv', 'field', 'side' );
	//add_meta_box('field_typediv',
	
}


// Register Options Taxonomy for checkboxes and dropdowns
function options() {

	$labels = array(
		'name'                       => _x( 'Options', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Option', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Options', 'text_domain' ),
		'all_items'                  => __( 'All Items', 'text_domain' ),
		'parent_item'                => __( 'Parent Item', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
		'new_item_name'              => __( 'New Item Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Item', 'text_domain' ),
		'edit_item'                  => __( 'Edit Item', 'text_domain' ),
		'update_item'                => __( 'Update Item', 'text_domain' ),
		'view_item'                  => __( 'View Item', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Items', 'text_domain' ),
		'search_items'               => __( 'Search Items', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No items', 'text_domain' ),
		'items_list'                 => __( 'Items list', 'text_domain' ),
		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true
	);
	register_taxonomy( 'option', array( 'field' ), $args );

}
add_action( 'init', 'options', 0 );








// Add custom fields for fields post type

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_field',
		'title' => 'field',
		'fields' => array (
			array (
				'key' => 'field_58069d15e0702',
				'label' => 'Field Instructions',
				'name' => 'instructions',
				'type' => 'textarea',
				'instructions' => 'Instructions for people, shown when editing their profiles.',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'br',
			),
			array (
				'key' => 'field_5806a5f95e4cd',
				'label' => 'Sub-Title',
				'name' => 'label_notes',
				'type' => 'text',
				'instructions' => 'Subtitle or Instructions that will appear below the label (title) of the field when people edit their profiles.',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'field',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}












?>