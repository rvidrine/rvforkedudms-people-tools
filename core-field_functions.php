<?php




function edudms_pt_fields_on_user_profile_editing_area($field_type) {
	$fields = get_pt_labels($field_type);
	
	if( $field_type == 'tab' ) {
			foreach($fields as $field) {
				$instructions = get_field('instructions', $field->ID);
				$subtitle = get_field('label_notes', $field->ID);
				global $user_id
				?><pre><?php
				//print_r($field);
				?></pre><?php
				//var_dump($field);
			?>
				<tr>
					<th>
						<label><?php echo $field->post_title; ?></label>
						<br>
						<span class="description"><?php echo $subtitle; ?></span>
					</th>
					<td>
						<?php 
							$db_key = sanitize_key('pt_' . $field->ID);
							wp_editor( get_the_author_meta( $db_key, $user_id ), $db_key ); 
						?>
						<br />
						<span class="description"><?php echo $instructions; ?></span>
					</td>
				</tr>
			<?php }
		}
	if( $field_type == 'dropdown' ) {
			foreach($fields as $field) {
				global $user_id;
				if($field != null ) {
					$instructions = get_field('instructions', $field->ID);
					$subtitle = get_field('label_notes', $field->ID);
				?>
					<tr>
					<th>
						<label><?php echo $field->post_title; ?></label>
						<br>
						<span class="description"><?php echo $subtitle; ?></span>
					</th>
					<td>

					<?php
					$values = wp_get_post_terms( $field->ID, 'option' );
					$current_value = get_user_meta( $GLOBALS["user_id"], 'pt_' . $field->ID );
					
					//var_dump($values);
					//var_dump($current_value);
										
					?>
					<select name="pt_<?php echo $field->ID; ?>" id="pt_<?php echo $field->ID; ?>">
					<option selected disabled>Choose one</option>
					<?php
					
					foreach($values as $value) { ?>
						<option value="<?php echo $value->term_id; ?>" <?php if($current_value[0] == $value->term_id) { echo 'selected="selected"'; }?>><?php echo $value->name; ?></option>
					<?php }
				   ?>
					</select> 

					<span class="description"><?php echo $instructions; ?></span>
						</td>
						</tr> 
				<?php }
									
								}
		}
	if( $field_type == 'checkbox' ) {
		foreach($fields as $field) {
				global $user_id;
				if($field != null ) {
					$instructions = get_field('instructions', $field->ID);
					$subtitle = get_field('label_notes', $field->ID);
				?>
					<tr>
					<th>
						<label><?php echo $field->post_title; ?></label>
						<br>
						<span class="description"><?php echo $subtitle; ?></span>
					</th>
					<td>
						<span class="description"><?php echo $instructions; ?></span>
						<?php
						$terms = wp_get_post_terms( $field->ID, 'option' );
						$current_terms_raw = get_user_meta( $GLOBALS["user_id"], 'pt_' . $field->ID );
						$current_terms = $current_terms_raw[0];
						
						
						
						
						//var_dump($current_terms);
						//var_dump($terms);
						
						foreach($terms as $term) {
							if( !empty($current_terms) ) {
								$checked_check = in_array($term->term_id, $current_terms);
							}
						?>
						
						<br><input type="checkbox" <?php checked( $checked_check, 1, true ); ?> id="<?php echo 'pt_' . $field->ID; ?>[]" name="<?php echo 'pt_' . $field->ID; ?>[]" value="<?php echo $term->term_id; ?>">
						<label for="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></label>
						
						
						<?php }
					   ?>
						</select> 

					
						</td>
						</tr> 
				<?php }
									
				}
		}
		if( $field_type == 'shortfield' ) {
		foreach($fields as $field) {
				global $user_id;
				if($field != null ) {
					$instructions = get_field('instructions', $field->ID);
					$subtitle = get_field('label_notes', $field->ID);
					$db_key = sanitize_key('pt_' . $field->ID);
					$value = get_the_author_meta( $db_key, $user_id );
				?>
					<tr>
					<th>
						<label><?php echo $field->post_title; ?></label>
						<br>
						<span class="description"><?php echo $subtitle; ?></span>
					</th>
					<td>
						
						<input type="text" id="<?php echo $db_key; ?>" name="<?php echo $db_key; ?>" value="<?php echo $value; ?>">
						
						<span class="description"><?php echo $instructions; ?></span>
						
						
						</td>
						</tr> 
				<?php }
									
				}
		}
	}




function update_pt_user_meta() {
	$labels = get_pt_labels();
	
	
	foreach($labels as $label) {
	global $user_id;
	update_user_meta( absint( $user_id ),  sanitize_key('pt_' . $label->ID), wp_kses_post( $_POST[sanitize_key('pt_' . $label->ID)] ) );
	}
}




function get_pt_field_info( $field, $return ) {
	$field_object = get_posts( array(
				'title'				=> $field,
				'post_type'   		=> 'field',
				'post_status' 		=> 'publish',
				'posts_per_page' 	=> -1,
						)
				);
	$info = $field_object[0]->$return;
	
	return $info;
}







//misc_tools for fields

function get_pt_labels($field_type = 'all') {
			if($field_type != 'all') {
				$labels = get_posts( array(
				'post_type'   		=> 'field',
				'post_status' 		=> 'publish',
				'posts_per_page' 	=> -1,
				'tax_query'			=> array( array (
											'taxonomy'	=> 'field_type',
											'terms'		=> $field_type,
											'field'		=> 'slug',
											'operator'	=> 'IN'
										) )
						)
				);
			}
			if($field_type == 'all' and $field == null) {
				$labels = get_posts( array(
				'post_type'   		=> 'field',
				'post_status' 		=> 'publish',
				'posts_per_page' 	=> -1,
						)
				);
			}
			
			
			return $labels;
}





function output_fields($user_id = null, $field_type = 'tab' ) {
	if(empty($user_id)==true){
		global $user_id;
	}
	$fields = get_pt_labels($field_type);
	//var_dump($fields);
	if($field_type == 'tab' ) {
		output_tabs($user_id, $fields);
	}
	if($field_type == 'checkbox' ) {
		output_checkboxes($user_id, $fields);
	}
	if($field_type == 'dropdown' ) {
		output_dropdowns($user_id, $fields);
	}
	if($field_type == 'shortfield' ) {
		output_shortfields($user_id, $fields);
	}
	
}

function output_tabs($user_id, $fields) {
	foreach($fields as $field){
		$db_key = sanitize_key('pt_' . $field->ID);
		//var_dump($field);
		$value = get_user_meta($user_id , $db_key, true);
		if(!empty($value)) {
			echo do_shortcode('[tabby title="' . $field->post_title . '"]');
			echo wpautop( do_shortcode( $value ));
		}
	}
}


function output_checkboxes($user_id, $fields) {
	foreach($fields as $field){
		$db_key = sanitize_key('pt_' . $field->ID);
		//var_dump($field);
		$value = get_user_meta($user_id , $db_key, true);
		if(!empty($value)) {
		?>
		<div id="edudms_pt_prp_contact_item_wrapper"><span class="label2"><?php echo esc_html( $field->post_title ); ?>:</span> <span class="edudms_pt_prp_value office">
			

			<?php
			$i = 0;
				$len = count($value);
				foreach($value as $value_checked) {
						$term = get_term( $value_checked, 'option' );
							echo $term->name;
						if ($i != $len - 1) {
							echo ', ';
						}
					$i++;
				}
			?>
			
			</span></div>
			
		<?php } }
	}

function output_dropdowns($user_id, $fields) {
	foreach($fields as $field){
		$db_key = sanitize_key('pt_' . $field->ID);
		//var_dump($field);
		$value = get_user_meta($user_id , $db_key, true);
		if(!empty($value)) {
		?>
		<div id="edudms_pt_prp_contact_item_wrapper"><span class="label2"><?php echo esc_html( $field->post_title ); ?>:</span> <span class="edudms_pt_prp_value office">
			

			<?php
					$term = get_term( $value, 'option' );
							echo $term->name;
						
			
			?>
			
			</span></div>
			
		<?php } }
	}

function output_shortfields($user_id, $fields) {
	foreach($fields as $field){
		$db_key = sanitize_key('pt_' . $field->ID);
		//var_dump($field);
		$value = get_user_meta($user_id , $db_key, true);
		if(!empty($value)) {
		?>
		<div id="edudms_pt_prp_contact_item_wrapper"><span class="label2"><?php echo esc_html( $field->post_title ); ?>:</span> <span class="edudms_pt_prp_value office">
			

			<?php
					
							echo $value;
						
			
			?>
			
			</span></div>
			
		<?php } }
	}


?>