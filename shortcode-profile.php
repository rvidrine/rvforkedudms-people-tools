<?php


add_shortcode( 'profile_page', 'show_profile_page' );


function show_profile_page() {
	//ob_start();
		?>

		<?php $edudms_pt_person = get_users( 'include=' . $_GET['user'] );?>

		<?php foreach ( array_slice($edudms_pt_person, 0, 1) as $user ) { 
			$useridentifier = $_GET["user"]; //receive user id from URL
			$first_name = $user->first_name;
			$last_name = $user->last_name;
			$title = $user->title;
			$email = $user->user_email;
			$website = $user->user_url;
			$phone = $user->phone;
			$office = $user->office; 
			$edudms_pt_member_type = $user->edudms_pt_member_type;

			
		?>






		<div class="edudms-entry-content">


			<div id="profArea">
				<div id="profName"><?php echo esc_html( $first_name . ' ' . $last_name ); ?></div>
				
				<div id="profPicArea">
					<div id="profPicIndent">
						<?php echo get_wp_user_avatar( $email, 250, 'left' ); ?>
					</div>
				</div>
				<div id="edudms_pt_prp_title"><?php echo esc_html( $title ); ?></div>
				<div class="edudms_pt_prp_contact_wrapper">
				
					<div id="edudms_pt_prp_contact_item_wrapper"><span class="label2">Office:</span> <span class="edudms_pt_prp_value office"><?php echo esc_html( $office ); ?></span></div>
					<div id="edudms_pt_prp_contact_item_wrapper"><span class="label2">Email:<span> <span class="edudms_pt_prp_value email"><?php echo '<a href="mailto:' . esc_attr( $email ) . '" >' . esc_html( $email ) . '</a>'; ?></span></div>
					<div id="edudms_pt_prp_contact_item_wrapper"><span class="label2">Phone:</span> <span class="edudms_pt_prp_value phone"><?php echo esc_html( $phone ); ?></span></div>

					<?php output_fields( $useridentifier, 'dropdown' ); ?>
					<?php output_fields( $useridentifier, 'shortfield' ); ?>
					<?php output_fields( $useridentifier, 'checkbox' ); ?>
					<div id="website">
						<?php
							if( $website ) {
							echo '<span class="label">Website:</span> <span class="profilefield website">' . '<a href="' . esc_url( $website ) . '">' . $website . '</a></span>'; }; 
						?>
					</div>

				</div> <!-- edudms_pt_prp_contact_wrapper -->
			</div> <!-- end of profArea -->
			
			
			
		<?php output_fields( $useridentifier, 'tab' ); ?>

		<?php echo do_shortcode('[tabbyending]'); ?>	


		<?php 
		
		
		}
		//$output_string = ob_get_contents();
    //ob_end_clean();
	//return $output_string;
}
 ?>