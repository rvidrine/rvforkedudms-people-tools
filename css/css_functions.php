<?php


function pt_css_register_submenu_page() {
	add_theme_page( 'EduDMS PT Appearance', 'EduDMS PT Appearance', 'edit_theme_options', 'pt_css', 'pt_css_render_submenu_page' );
}
add_action( 'admin_menu', 'pt_css_register_submenu_page' );



function pt_css_register_style() {
	$url = home_url();

	if ( is_ssl() ) {
		$url = home_url( '/', 'https' );
	}

	wp_register_style( 'pt_css_style', add_query_arg( array( 'pt_css' => 1 ), $url ) );

	wp_enqueue_style( 'pt_css_style' );
}
add_action( 'wp_enqueue_scripts', 'pt_css_register_style', 99 );

/**
 * If the query var is set, print the Simple Custom CSS rules.
 */
function pt_css_maybe_print_css() {

	// Only print CSS if this is a stylesheet request
	if( ! isset( $_GET['pt_css'] ) || intval( $_GET['pt_css'] ) !== 1 ) {
		return;
	}

	ob_start();
	header( 'Content-type: text/css' );
	$options     = get_option( 'pt_css_settings' );
	$raw_cont = isset( $options['pt_css-content'] ) ? $options['pt_css-content'] : '';
	$content     = wp_kses( $raw_cont, array( '\'', '\"' ) );
	$content     = str_replace( '&gt;', '>', $content );
	echo $content;
	die();
}

add_action( 'plugins_loaded', 'pt_css_maybe_print_css' );



?>