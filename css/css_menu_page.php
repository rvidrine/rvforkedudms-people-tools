<?php


function pt_css_register_settings() {
	add_settings_section(
		'pt_css_settings_group',
		'People Editing Area Options',
		'pt_css_settings_group_callback_function',
		'pt_css_options_page'
	);
	
	register_setting( 'pt_css_settings_group', 'pt_css_settings' );
	register_setting( 'pt_css_settings_group', 'pt_css_style_setting' );
	
	add_settings_field(
		'pt_css_style_setting',
		'Show Bio Tab',
		'pt_css_style_setting_callback_function',
		'pt_css_options_page',
		'pt_css_style_settings_group'
	);
	
}
add_action( 'admin_init', 'pt_css_register_settings' );


function pt_css_style_setting_callback_function () {

}


function pt_css_render_submenu_page() {

	$options = get_option( 'pt_css_settings' );
	$content = isset( $options['pt_css-content'] ) && ! empty( $options['pt_css-content'] ) ? $options['pt_css-content'] : __( $GLOBALS["initial_pt_css"], 'pt-css' );

	if ( isset( $_GET['settings-updated'] ) ) : ?>
		<div id="message" class="updated"><p>Custom CSS updated successfully.</p></div>
	<?php endif; ?>
	
	
	
	
	<div class="wrap">
		<h2 style="margin-bottom: 1em;">CSS Settings</h2>
		<form name="pt_css-form" action="options.php" method="post" enctype="multipart/form-data">
			<?php settings_fields( 'pt_css_settings_group' ); ?>
			<div id="templateside">
				<?php do_action( 'pt_css-sidebar-top' ); ?>
				<p style="margin-top: 0">Adjust the css of the People Tools addon</p>
				<p>To use, read comments carefully, alter hex values (#???), then click "Update PT CSS".</p>
				
				
				
				<?php submit_button( 'Update PT CSS', 'primary', 'submit', true ); ?>
				
				
				
				<?php do_action( 'pt_css-sidebar-bottom' ); ?>
			</div>
			<div id="template">
				<?php do_action( 'pt_css-form-top' ); ?>
				<div>
					<textarea cols="70" rows="30" name="pt_css_settings[pt_css-content]" id="pt_css_settings[pt_css-content]" ><?php echo esc_html( $content ); ?></textarea>
					
				</div>
				<?php do_action( 'pt_css-textarea-bottom' ); ?>
				<div>
					<?php submit_button( 'Update PT CSS', 'primary', 'submit', true ); ?>
				</div>
				<?php do_action( 'pt_css-form-bottom' ); ?>
			</div>
		</form>
		
	</div>


	

	
<?php
}





//Initial CSS

$initial_pt_css = '

/* List Format CSS */
.pp-header-block {
	background-color: #fff; /* Header Row Background Color */
}
div.person-block:hover {
	background-color: #fff; /* Row Highlight Color */
}
div.email {
/* Alignment of Email in List Format, Possible Values: */
text-align: left; /* Possible Values: left, center */
}






';





?>