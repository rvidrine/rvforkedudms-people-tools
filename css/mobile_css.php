<?php

header("Content-type: text/css; charset: UTF-8");


// faster load by reducing memory with SHORTINIT
define('SHORTINIT', true);

// recursively find WordPress load
function find_require($file,$folder=null) {
    if ($folder === null) {$folder = dirname(__FILE__);}
    $path = $folder.DIRECTORY_SEPARATOR.$file;
    if (file_exists($path)) {require($path); return $folder;}
    else {
        $upfolder = find_require($file,dirname($folder));
        if ($upfolder != '') {return $upfolder;}
    }
}

// load WordPress core (minimal)
$wp_root_path = find_require('wp-load.php');
define('ABSPATH', $wp_root_path);
define('WPINC', 'wp-includes');

?>








<?php









?>