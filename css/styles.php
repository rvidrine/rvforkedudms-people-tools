<?php


function pt_css_register_submenu_page() {
	add_theme_page( 'EduDMS PT Appearance', 'PT Appearance', 'edit_theme_options', basename( pt_css_FILE ), 'pt_css_render_submenu_page' );
}
add_action( 'admin_menu', 'pt_css_register_submenu_page' );




function pt_css_register_settings() {
	register_setting( 'pt_css_settings_group', 'pt_css_settings' );
}
add_action( 'admin_init', 'pt_css_register_settings' );


function pt_css_render_submenu_page() {

	$options = get_option( 'pt_css_settings' );
	$content = isset( $options['pt_css-content'] ) && ! empty( $options['pt_css-content'] ) ? $options['pt_css-content'] : __( '/* Enter Your Custom CSS Here */', 'pt-css' );

	if ( isset( $_GET['settings-updated'] ) ) : ?>
		<div id="message" class="updated"><p>Custom CSS updated successfully.</p></div>
	<?php endif; ?>
	<div class="wrap">
		<h2 style="margin-bottom: 1em;">CSS Settings</h2>
		<form name="pt_css-form" action="options.php" method="post" enctype="multipart/form-data">
			<?php settings_fields( 'pt_css_settings_group' ); ?>
			<div id="templateside">
				<?php do_action( 'pt_css-sidebar-top' ); ?>
				<p style="margin-top: 0">Adjust the css of the news addon</p>
				<p>To use, read comments carefully, alter hex values (#???), then click "Update PT CSS".</p>
				<?php submit_button( 'Update PT CSS', 'primary', 'submit', true ); ?>
				<?php do_action( 'pt_css-sidebar-bottom' ); ?>
			</div>
			<div id="template">
				<?php do_action( 'pt_css-form-top' ); ?>
				<div>
					<textarea cols="70" rows="30" name="pt_css_settings[pt_css-content]" id="pt_css_settings[pt_css-content]" ><?php echo esc_html( $content ); ?></textarea>
				</div>
				<?php do_action( 'pt_css-textarea-bottom' ); ?>
				<div>
					<?php submit_button( 'Update PT CSS', 'primary', 'submit', true ); ?>
				</div>
				<?php do_action( 'pt_css-form-bottom' ); ?>
			</div>
		</form>
		
	</div>

<?php
}



function pt_css_register_style() {
	$url = home_url();

	if ( is_ssl() ) {
		$url = home_url( '/', 'https' );
	}

	wp_register_style( 'pt_css_style', add_query_arg( array( 'pt_css' => 1 ), $url ) );

	wp_enqueue_style( 'pt_css_style' );
}
add_action( 'wp_enqueue_scripts', 'pt_css_register_style', 99 );

/**
 * If the query var is set, print the Simple Custom CSS rules.
 */
function pt_css_maybe_print_css() {

	// Only print CSS if this is a stylesheet request
	if( ! isset( $_GET['pt_css'] ) || intval( $_GET['pt_css'] ) !== 1 ) {
		return;
	}

	ob_start();
	header( 'Content-type: text/css' );
	$options     = get_option( 'pt_css_settings' );
	$raw_cont = isset( $options['pt_css-content'] ) ? $options['pt_css-content'] : '';
	$content     = wp_kses( $raw_cont, array( '\'', '\"' ) );
	$content     = str_replace( '&gt;', '>', $content );
	echo $content;
	die();
}

add_action( 'plugins_loaded', 'pt_css_maybe_print_css' );



?>